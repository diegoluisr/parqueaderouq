import co.edu.uniquindio.parking.*;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        Universidad universidad = new Universidad();

        universidad.getParqueaderos().add(new Parqueadero(TipoParqueadero.ADMIN_BASICAS));
        universidad.getParqueaderos().add(new Parqueadero(TipoParqueadero.INGENIERIAS));

        Parqueadero ingenierias = universidad.getParquederoById(TipoParqueadero.INGENIERIAS);
        Parqueadero basicas = universidad.getParquederoById(TipoParqueadero.INGENIERIAS);

        ingenierias.getVehiculos().add(new Carro("JRV402", new Date()));

        System.out.println(ingenierias.countVehicleByType("carro"));
        System.out.println(ingenierias.countVehicleByType("moto"));

        Vehiculo carro = ingenierias.removeByPlaca("JRV402", new Date());
        System.out.println(carro);
    }
}
