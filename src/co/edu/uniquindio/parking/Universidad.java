package co.edu.uniquindio.parking;

import java.util.ArrayList;

public class Universidad {

    private ArrayList<Parqueadero> parqueaderos;

    public Universidad() {
        parqueaderos = new ArrayList<>();
    }

    public ArrayList<Parqueadero> getParqueaderos() {
        return parqueaderos;
    }

    public void setParqueaderos(ArrayList<Parqueadero> parqueaderos) {
        this.parqueaderos = parqueaderos;
    }

    public Parqueadero getParquederoById(TipoParqueadero tipo) {
        for (Parqueadero item: parqueaderos) {
            if (item.getTipoParqueadero().equals(tipo)) {
                return item;
            }
        }
        return null;
    }
}
