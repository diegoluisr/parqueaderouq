package co.edu.uniquindio.parking;

import java.util.ArrayList;
import java.util.Date;

public class Parqueadero {

    private TipoParqueadero tipo;
    private int limiteMoto = 50;
    private int limiteCarro = 50;
    private int limiteEspecial = 50;

    public final String CARRO = "carro";
    public final String MOTO = "moto";
    public final String ESPECIAL = "especial";

    private ArrayList<Vehiculo> vehiculos;

    public Parqueadero(TipoParqueadero tipo) {
        this.tipo = tipo;
        vehiculos = new ArrayList<>();
    }

    public TipoParqueadero getTipoParqueadero() {
        return tipo;
    }

    public int getLimite(String tipo) {
        if (tipo.equals(CARRO)) {
            return limiteCarro;
        }
        else if (tipo.equals(MOTO)) {
            return limiteMoto;
        }
        else if (tipo.equals(ESPECIAL)) {
            return limiteEspecial;
        }
        return 0;
    }

    public void setLimite(String tipo, int limite) {
        if (tipo.equals(CARRO)) {
            limiteCarro = limite;
        }
        else if (tipo.equals(MOTO)) {
            limiteMoto = limite;
        }
        else if (tipo.equals(ESPECIAL)) {
            limiteEspecial = limite;
        }
    }

    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public Vehiculo removeByPlaca(String placa, Date salida) {
        for (Vehiculo item:vehiculos) {
            if (item.getPlaca().equals(placa)) {
                item.setSalida(salida);
                vehiculos.remove(item);
                return item;
            }
        }
        return null;
    }

    public int countVehicleByType(String tipo) {
        int count = 0;
        for (Vehiculo vehiculo: vehiculos) {
            if (tipo.equals("carro") && vehiculo.getClass().equals(Carro.class)) {
                count++;
            }
            else if (tipo.equals("moto") && vehiculo.getClass().equals(Moto.class)) {
                count++;
            }
            else if (tipo.equals("especial") && vehiculo.getClass().equals(Especial.class)) {
                count++;
            }
        }
        return count;
    }
}
