package co.edu.uniquindio.parking;

import java.util.Date;

public class Vehiculo {

    private String placa;
    private Date ingreso;
    private Date salida;

    public Vehiculo(String placa, Date ingreso) {
        this.placa = placa;
        this.ingreso = ingreso;
    }

    public String getPlaca() {
        return placa;
    }

    public Date getIngreso() {
        return ingreso;
    }

    public Date getSalida() {
        return salida;
    }

    public void setSalida(Date salida) {
        this.salida = salida;
    }

    @Override
    public String toString() {
        return "Vehiculo: placa (" + this.placa + ")";
    }

}
